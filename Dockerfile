FROM python:3.9-slim
RUN pip install --no-cache-dir pykmip
COPY ./data /work
WORKDIR /work
CMD pykmip-server -f /work/conf/server.conf -l /work/logs/server.log